:: Optionally install a copy of this SciTE as a text editor under "Program Files"
:: or "Program Files (x86)". Then, optionally, add the resulting SciTE directory
:: to the path. Finally, also optionally, set it as the editor for .bat files.

:: On Windows 7+ you will need to "Run as administrator" rather than just run
:: this file.

:: Note that the changes to environment variables won't be initially recognized. You
:: need to restart, or go to Control Panel->System->Advanced->Environment variables
:: and click OK, or use some binary that "broadcasts a window message", as explained
:: in http://stackoverflow.com/questions/24500881 and
::    http://stackoverflow.com/questions/20653028

@set "dest=%PROGRAMFILES%"
@if "%PROGRAMFILES(x86)%" == "" goto end32
:start32
@set "dest=%PROGRAMFILES(x86)%"
:end32


@set "EnvKey=HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Session Manager\Environment"

@FOR /f "usebackq tokens=2*" %%a IN (`REG QUERY "%EnvKey%" /v Path`) do @set "OldPath=%%~b"

@set "dest=%dest%\SciTE"
@if exist "%dest%" goto endmkdir
:startmkdir
@mkdir "%dest%"
:endmkdir
@copy /-Y %~dp0\scite.exe              "%dest%\"
@copy /-Y %~dp0\SciTEGlobal.properties "%dest%\"

@echo Adding %dest% to Path environment variable:
@reg add "%EnvKey%" /v Path /t REG_EXPAND_SZ /d "%OldPath%;%dest%

@echo Setting %dest%\scite.exe as default editor for .bat files:
@reg add "HKEY_CLASSES_ROOT\batfile\shell\edit\command" /ve /d "%dest%\scite.exe %%1"

@echo Setting %dest%\scite.exe as default editor for .cmd files:
@reg add "HKEY_CLASSES_ROOT\cmdfile\shell\edit\command" /ve /d "%dest%\scite.exe %%1"

@set /p ANS="Should the command `edit` invoke scite? (y/n): "
@if /I "%ANS%"=="y" echo @start scite %%* > "%dest%\edit.cmd"


@pause
@SystemPropertiesAdvanced
