:: On Windows 7+ you will need to "Run as administrator" rather than just run
:: this file.
:: 
:: Note that the changes to environment variables won't be initially recognized. You
:: need to restart, or go to Control Panel->System->Advanced->Environment variables
:: and click OK, or use some binary that "broadcasts a window message", as explained
:: in http://stackoverflow.com/questions/24500881 and
::    http://stackoverflow.com/questions/20653028

@set "Application=%1
@set "DefaultApplication=BCI2000
@if "%Application%"=="" set /P Application="Application name (default: %DefaultApplication%): "
@if "%Application%"=="" set "Application=%DefaultApplication%

@cd "%0\..
@set "AppDir=%CD%
@set "EnvKey=HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Session Manager\Environment"
@set "VarName=PYTHONHOME_%Application%

@echo.Setting system environment variable:
@echo.   %VarName% = %AppDir%

@reg add "%EnvKey%" /v "%VarName%" /t REG_EXPAND_SZ /d "%AppDir%

@pause
@SystemPropertiesAdvanced
